=== UMW Progressive Web App ===
Contributors: cgrymala
Donate link: https://www.umw.edu/
Tags: pwa, umw
Requires at least: 4.4
Tested up to: 4.9.8
Stable tag: 0.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin provides a basic structure for a progressive web app for the UMW website

== Description ==

This plugin implements the basic structure of a progressive web app to make parts of the UMW website locally available even during outages and interruptions.

== Installation ==

1. Upload `umw-pwa.php` and the `lib` subdirectory to the `/wp-content/mu-plugins/` directory
1. Copy `lib/scripts/umw/pwa/sw.js` to the root directory of your website

== Changelog ==

= 0.1.0 =
* First version

== Upgrade Notice ==

= 0.1.0 =
This is the first version of the plugin
