<?php
/**
 * Plugin Name:     UMW Progressive Web App
 * Plugin URI:      https://www.umw.edu/
 * Description:     A basic PWA for use on the UMW website
 * Author:          Curtiss Grymala
 * Author URI:      https://www.umw.edu/
 * Text Domain:     umw-pwa
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Umw_Pwa
 */
 add_action( 'wp_head', 'insert_umw_pwa_manifest' );
 function insert_umw_pwa_manifest() {
   $location = plugins_url( 'lib/scripts/umw/pwa/manifest.json', __FILE__ );
   printf( '<link rel="manifest" href="%s"/>', $location );
 }

 add_action( 'wp_print_footer_scripts', 'invoke_umw_sw' );
 function invoke_umw_sw() {
 $script = <<<EOF
   if ('serviceWorker' in navigator) {
     window.addEventListener('load', function() {
       navigator.serviceWorker.register('/sw.js').then(function(registration) {
         // Registration was successful
         console.log('ServiceWorker registration successful with scope: ', registration.scope);
       }, function(err) {
         // registration failed :(
         console.log('ServiceWorker registration failed: ', err);
       });
     });
   }
 EOF;

 printf( '<script>%s</script>', $script );
 }
